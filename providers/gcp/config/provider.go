package config

type Provider struct {
	ServiceAccountFile string `toml:"ServiceAccountFile"`

	Project           string `toml:"Project"`
	Zone              string `toml:"Zone"`
	MachineType       string `toml:"MachineType"`
	MinCPUPlatform    string `toml:"MinCPUPlatform"`
	Image             string `toml:"Image"`
	DiskSize          int64  `toml:"DiskSize"`
	DiskType          string `toml:"DiskType"`
	Network           string `toml:"Network"`
	Subnetwork        string `toml:"Subnetwork"`
	UseInternalIPOnly bool   `toml:"UseInternalIPOnly"`

	Tags   []string          `toml:"Tags"`
	Labels map[string]string `toml:"Labels"`

	Username string `toml:"Username"`
}
