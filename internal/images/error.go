package images

import "fmt"

type ImageNotMatchingPatternError struct {
	image string
}

func NewImageNotMatchingPatternError(image string) *ImageNotMatchingPatternError {
	return &ImageNotMatchingPatternError{image: image}
}

func (err *ImageNotMatchingPatternError) Error() string {
	return fmt.Sprintf("job image %q does not match allowed image patterns. Check the AllowedImages property in your config.", err.image)
}
